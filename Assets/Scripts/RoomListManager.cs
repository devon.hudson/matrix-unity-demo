﻿/*============================================================================*/
/*                           This file is part of:                            */
/*                               Matrix Unity                                 */
/*============================================================================*/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2020 Devon Hudson                                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/*============================================================================*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class RoomListManager : MonoBehaviour
{
    public Action<string> PropagateSelectedRoom;

    [SerializeField]
    private GameObject roomTemplate;

    private List<GameObject> chatRooms = new List<GameObject>();

    void Start()
    {
    }

    public void AddRoom(string roomID, string roomName)
    {
        GameObject newRoom = Instantiate(roomTemplate) as GameObject;
        newRoom.SetActive(true);

        newRoom.GetComponent<ChatRoom>().SetName(roomName);
        newRoom.GetComponent<ChatRoom>().SetID(roomID);
        newRoom.transform.SetParent(roomTemplate.transform.parent, false);
        newRoom.GetComponent<ChatRoom>().PropagateSelectedRoom += HandlePropagateSelectedRoom;

        chatRooms.Add(newRoom.gameObject);
    }

    public void Clear()
    {
        foreach(GameObject go in chatRooms)
        {
            Destroy(go);
        }
        chatRooms.Clear();
    }

    public void HandlePropagateSelectedRoom(string roomName)
    {
        PropagateSelectedRoom?.Invoke(roomName);
    }
}

