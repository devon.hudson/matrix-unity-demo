﻿/*============================================================================*/
/*                           This file is part of:                            */
/*                               Matrix Unity                                 */
/*============================================================================*/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2020 Devon Hudson                                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/*============================================================================*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ChatRoom : MonoBehaviour
{
    public Action<string> PropagateSelectedRoom;
    private string roomID;

    public void SetName(string name)
    {
        GetComponentInChildren<Text>().text = string.Format(name);
        GetComponentInChildren<Text>().color = new Color(0.2f, 0.2f, 0.2f, 1f);
    }

    public void SetID(string roomID)
    {
        this.roomID = roomID;
    }

    public void OnSelect()
    {
        PropagateSelectedRoom?.Invoke(roomID);
    }
}
