﻿/*============================================================================*/
/*                           This file is part of:                            */
/*                               Matrix Unity                                 */
/*============================================================================*/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2020 Devon Hudson                                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/*============================================================================*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public enum MessageLocation
{
    FRONT,
    BACK
}

public class ChatroomManager : MonoBehaviour
{
    [SerializeField]
    private GameObject messageTemplate;

    public Action<string, string> SendMessageToRoom;

    [SerializeField]
    private InputField inputField;

    private Dictionary<string /* RoomID */, List<GameObject> /* Messages */> knownMessages = new Dictionary<string, List<GameObject>>();
    private List<GameObject> roomMessages = new List<GameObject>();
    private string selectedRoomID = "";

    void Start()
    {

    }

    public void SelectRoom(string roomID)
    {
        RefreshActiveMessageList(roomID, selectedRoomID);
        selectedRoomID = roomID;
    }

    public void AddMessage(string roomID, string eventID, MessageLocation location, string text, string user, System.DateTime timestamp)
    {
        if(knownMessages.ContainsKey(roomID))
        {
            foreach(var message in knownMessages[roomID])
            {
                if(eventID == message.GetComponent<ChatMessage>().GetEventID())
                {
                    Debug.Log("Event ID already known, dropping message.");
                    return;
                }
            }
        }
        else
        {
            // Create new room for message caching
            knownMessages.Add(roomID, new List<GameObject>());
        }

        GameObject newMessage = Instantiate(messageTemplate) as GameObject;

        // TODO : Can I get away with only setting specific messages as active?
        // or do I need to spin up different gameobjects when you switch message rooms?
        // Don't want to be abusing the garbage collector too much here.
        // also think about how this translates into the other game engines
        // NOTE : flipping isActive seems to kind of work except some messages seem
        // to disappear when changing between rooms
        bool setActive = (roomID == selectedRoomID) ? true : false;
        newMessage.SetActive(setActive);

        newMessage.GetComponent<ChatMessage>().SetMessage(eventID, text, user, timestamp);
        newMessage.transform.SetParent(messageTemplate.transform.parent, false);

        if(location == MessageLocation.FRONT)
        {
            newMessage.transform.SetAsLastSibling();
            roomMessages.Add(newMessage.gameObject);
            knownMessages[roomID].Add(newMessage.gameObject);
        }
        else
        {
            newMessage.transform.SetAsFirstSibling();
            roomMessages.Insert(0, newMessage.gameObject);
            knownMessages[roomID].Insert(0, newMessage.gameObject);
        }
    }

    public void Clear()
    {
        foreach(GameObject go in roomMessages)
        {
            Destroy(go);
        }
        roomMessages.Clear();

        foreach(var room in knownMessages.Values)
        {
            foreach(GameObject go in room)
            {
                Destroy(go);
            }
            room.Clear();
        }
        roomMessages.Clear();
    }

    public void InvokeSendMessageToRoom()
    {
        if(selectedRoomID != string.Empty)
        {
            Debug.Log("Sending message: " + inputField.text + " --- to room: " + selectedRoomID);
            SendMessageToRoom?.Invoke(selectedRoomID, inputField.text);
            inputField.text = string.Empty;
        }
        else
        {
            Debug.LogError("No room selected. Not sending message.");
        }
    }

    private void RefreshActiveMessageList(string newRoomID, string previousRoomID)
    {
        if(knownMessages.ContainsKey(previousRoomID))
        {
            foreach(GameObject message in knownMessages[previousRoomID])
            {
                message.SetActive(false);
            }
        }

        if(knownMessages.ContainsKey(newRoomID))
        {
            foreach(GameObject message in knownMessages[newRoomID])
            {
                message.SetActive(true);
            }
        }
    }
}
