﻿/*============================================================================*/
/*                           This file is part of:                            */
/*                               Matrix Unity                                 */
/*============================================================================*/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2020 Devon Hudson                                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/*============================================================================*/

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIManager : MonoBehaviour
{
    public GameObject loginPanel;
    public GameObject loadingPanel;
    public GameObject successPanel;
    public GameObject chatPanel;

    // TODO : Remove these references
    public ChatroomManager chatroom;
    public RoomListManager roomList;

    private Matrix.IMatrixClient matrixClient;
    private IEnumerator timeoutCoroutine;
    private const int SYNC_POLL_TIMEOUT_S = 30;
    private string roomID = "";

    public void RetrievePreviousMessages()
    {
        Matrix.RoomMessagesSearchParameters search = new Matrix.RoomMessagesSearchParameters();
        search.searchDirection = Matrix.SearchDirection.BACKWARD;
        search.roomID = roomID;
        matrixClient?.GetRoomMessages(search);
    }

    public void Logout()
    {
        matrixClient.Logout();
    }

    // Start is called before the first frame update
    void Start()
    {
        // Setup Matrix Client
        matrixClient = FindObjectOfType<MatrixClient>();
        matrixClient.LoginFinished += OnLoginFinished;
        matrixClient.LogoutFinished += OnLogoutFinished;
        matrixClient.SyncFinished += OnSyncFinished;
        matrixClient.RetrievedJoinedRooms += OnRetrievedJoinedRooms;
        matrixClient.RetrievedRoomName += OnRetrievedRoomName;
        matrixClient.RetrievedRoomMessages += OnRetrievedRoomMessages;
        matrixClient.RetrievedDisplayName += OnRetrievedDisplayName;

        loginPanel?.SetActive(true);
        loadingPanel?.SetActive(false);
        successPanel?.SetActive(false);

        loginPanel.GetComponent<LoginPanel>().Login += OnLoginInitiated;
    }

    void OnLoginInitiated(string username, string password, string homeserver)
    {
        loginPanel?.SetActive(false);
        loadingPanel?.SetActive(true);
        Matrix.LoginParameters parameters = new Matrix.LoginParameters();
        parameters.username = username;
        parameters.password = password;
        parameters.homeserver = homeserver;
        matrixClient.Login(parameters);
    }

    void OnLoginFinished(bool success)
    {
        Debug.Log("Login result was: " + success);
        if(success)
        {
            successPanel?.SetActive(true);
            chatPanel?.SetActive(true);
            matrixClient.StartSyncPoll(SYNC_POLL_TIMEOUT_S);
            matrixClient.GetJoinedRooms();
            chatroom.SendMessageToRoom += HandleSendMessageToRoom;
        }
        else
        {
            loginPanel.SetActive(true);
        }

        loadingPanel?.SetActive(false);
    }

    void OnLogoutFinished(bool success)
    {
        Debug.Log("Logout result was: " + success);
        if(success)
        {
            chatroom.SendMessageToRoom -= HandleSendMessageToRoom;
            chatroom.Clear();
            roomList.Clear();

            loginPanel.SetActive(true);
            chatPanel?.SetActive(false);
            successPanel?.SetActive(false);
        }
    }

    // TODO : Add rooms list to UI and add messages to log in UI
    void OnSyncFinished(Matrix.SyncResponse response)
    {
        Debug.Log("Sync finished!");
        string result = "Account Events: \n";
        foreach(var item in response.account_data.events)
        {
            result += item.type + "\n";
        }
        Debug.Log(result);

        string result2 = "Joined Rooms: \n";
        foreach(KeyValuePair<string, Matrix.JoinedRoom> kvp in response.rooms.join)
        {
            result2 += kvp.Key + "\n";
        }
        Debug.Log(result2);
        Debug.Log("asdf");

        Debug.Log("Joined room count: " + response.rooms.join.Count);

        foreach(KeyValuePair<string, Matrix.JoinedRoom> room in response.rooms.join)
        {
            Debug.Log("Member Count: " + room.Value.summary.joined_member_count);

            foreach(var matrixEvent in room.Value.timeline.events)
            {
                Debug.Log("Events: " + matrixEvent.type);
                if(matrixEvent.content is Matrix.TextMessageContent)
                {
                    Matrix.TextMessageContent textMessage = (Matrix.TextMessageContent)matrixEvent.content;
                    System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                    dtDateTime = dtDateTime.AddMilliseconds(matrixEvent.origin_server_ts).ToLocalTime();
                    chatroom.AddMessage(room.Key, matrixEvent.event_id, MessageLocation.FRONT, textMessage.body, matrixEvent.sender, dtDateTime);
                }
            }
        }
    }

    void OnRetrievedJoinedRooms(Matrix.JoinedRoomsResponse joinedRooms)
    {
        string result = "Joined Rooms: \n";
        foreach (var item in joinedRooms.joined_rooms)
        {
            result += item + "\n";
            Matrix.RoomNameSearchParameters search = new Matrix.RoomNameSearchParameters();
            search.roomID = item;
            matrixClient.GetRoomName(search);

            // TODO : Remove
            roomID = item;
        }
        Debug.Log(result);
    }

    void OnRetrievedRoomName(Tuple<Matrix.RoomNameSearchParameters, Matrix.RoomNameResponse> roomName)
    {
        Debug.Log(string.Format("Room Name [{0}]: {1}", roomName.Item1.roomID, roomName.Item2.name));
        roomList.AddRoom(roomName.Item1.roomID, roomName.Item2.name);
    }

    void OnRetrievedRoomMessages(Tuple<Matrix.RoomMessagesSearchParameters, Matrix.RoomMessagesResponse> messages)
    {
        foreach(Matrix.RoomEvent roomEvent in messages.Item2.chunk)
        {
            if(roomEvent.content is Matrix.TextMessageContent)
            {
                Matrix.TextMessageContent textMessage = (Matrix.TextMessageContent)roomEvent.content;
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddMilliseconds(roomEvent.origin_server_ts).ToLocalTime();
                chatroom.AddMessage(roomEvent.room_id, roomEvent.event_id, MessageLocation.BACK, textMessage.body, roomEvent.sender, dtDateTime);

                Matrix.DisplayNameSearchParameters parameters = new Matrix.DisplayNameSearchParameters();
                parameters.userID = roomEvent.sender;
                matrixClient.GetUserDisplayName(parameters);
            }
        }
    }

    void OnRetrievedDisplayName(Tuple<Matrix.DisplayNameSearchParameters, Matrix.DisplayNameResponse> displayName)
    {
        Debug.Log(string.Format("Display Name [{0}]: {1}", displayName.Item1.userID, displayName.Item2.displayname));
    }

    public IEnumerator LoginTimeout(float timeout)
    {
        yield return new WaitForSeconds(timeout);
        if(!matrixClient.IsLoggedIn())
        {
            Debug.LogError("Login Failed due to timeout.");
            loginPanel?.SetActive(true);
            loadingPanel?.SetActive(false);
        }
    }

    private void HandleSendMessageToRoom(string roomID, string message)
    {
        matrixClient.PostRoomMessage(roomID, message);
    }
}
